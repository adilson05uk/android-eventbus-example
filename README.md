# GreenRobot EventBus Android Application Example

This project demonstrates how to setup and use the GreenRobot EventBus library in an Android application.

Refer to the application's [build.gradle](app/build.gradle) file for the required dependencies.

Refer to the [JobExecutor](app/src/main/java/com/thinkincode/eventbus/job/base/JobExecutor.java) class for a helper class that uses the `EventBus` to move requests (in the form of [Job](app/src/main/java/com/thinkincode/eventbus/job/base/Job.java) instances) from the requesting thread to be executed in a background thread.

Refer to the [MainActivity](app/src/main/java/com/thinkincode/eventbus/activity/MainActivity.java) class for an example of how to pass a [Job](app/src/main/java/com/thinkincode/eventbus/job/base/Job.java) instance to a [JobExecutor](app/src/main/java/com/thinkincode/eventbus/job/base/JobExecutor.java) instance and how to receive the result of the [Job](app/src/main/java/com/thinkincode/eventbus/job/base/Job.java) execution (in the form of a [JobResult](app/src/main/java/com/thinkincode/eventbus/job/base/JobResult.java) instance) on the main thread.

Lastly, refer to the [MyApplication](app/src/main/java/com/thinkincode/eventbus/MyApplication.java) class for an example of how to setup singleton instances of the [JobExecutor](app/src/main/java/com/thinkincode/eventbus/job/base/JobExecutor.java) and `EventBus` classes to be used across the application.