package com.thinkincode.eventbus;

import android.app.Application;

import com.thinkincode.eventbus.job.JobExecutor;

import org.greenrobot.eventbus.EventBus;

public class MyApplication extends Application {

    private static MyApplication instance;

    private JobExecutor jobExecutor;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        EventBus eventBus = EventBus.builder().addIndex(new EventBusIndex()).build();

        jobExecutor = new JobExecutor(eventBus);
        jobExecutor.onCreate();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        jobExecutor.onTerminate();
    }

    public static MyApplication getInstance() {
        return instance;
    }

    public JobExecutor getJobExecutor() {
        return jobExecutor;
    }
}