package com.thinkincode.eventbus.job;

import android.support.annotation.NonNull;

/**
 * Represents the result of a {@link Job} execution.
 *
 * @param <V> the type of value contained within the {@link JobResult}.
 */
public class JobResult<V> {

    @NonNull
    private final V value;

    public JobResult(@NonNull V value) {
        this.value = value;
    }

    @NonNull
    public V getValue() {
        return value;
    }
}
