package com.thinkincode.eventbus.job.b;

import android.support.annotation.NonNull;

import com.thinkincode.eventbus.job.Job;
import com.thinkincode.eventbus.job.JobResult;

public class JobB implements Job<JobB.Result> {

    @NonNull
    @Override
    public Result execute() {
        try {
            Thread.sleep(1000);
            return new Result("JobB complete!");
        } catch (InterruptedException e) {
            return new Result("Failed JobB!");
        }
    }

    public static class Result extends JobResult<String> {

        Result(@NonNull String message) {
            super(message);
        }
    }
}
