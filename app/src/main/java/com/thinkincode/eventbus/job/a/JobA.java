package com.thinkincode.eventbus.job.a;

import android.support.annotation.NonNull;

import com.thinkincode.eventbus.job.Job;
import com.thinkincode.eventbus.job.JobResult;

public class JobA implements Job<JobA.Result> {

    @NonNull
    @Override
    public Result execute() {
        try {
            Thread.sleep(3000);
            return new Result("JobA complete!");
        } catch (InterruptedException e) {
            return new Result("Failed JobA!");
        }
    }

    public static class Result extends JobResult<String> {

        Result(@NonNull String value) {
            super(value);
        }
    }
}
