package com.thinkincode.eventbus.job;

import android.support.annotation.NonNull;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Posts {@link Job} instances to be executed in a background thread
 * and posts the result of the job execution to be picked up on the main thread.
 */
public class JobExecutor {

    @NonNull
    private final EventBus eventBus;

    public JobExecutor(@NonNull EventBus eventBus) {
        this.eventBus = eventBus;
    }

    /**
     * Starts up this {@link JobExecutor} instance.
     */
    public void onCreate() {
        eventBus.register(this);
    }

    /**
     * Tears down this {@link JobExecutor} instance.
     */
    public void onTerminate() {
        eventBus.unregister(this);
    }

    /**
     * Registers the given subscriber to receive {@link JobResult} events.
     *
     * @param subscriber the {@link Object} to register.
     */
    public void register(@NonNull Object subscriber) {
        eventBus.register(subscriber);
    }

    /**
     * Unregisters the given subscriber so that it no longer receives {@link JobResult} events.
     *
     * @param subscriber the {@link Object} to unregister.
     */
    public void unregister(@NonNull Object subscriber) {
        eventBus.unregister(subscriber);
    }

    /**
     * Posts the given {@link Job} instance on the event bus
     * for it to be picked up and executed in a background thread.
     * <p>
     * The result of the {@link Job} execution will be posted back on the event bus
     * for it to be picked up by any subscribers registered to this class.
     *
     * @param job the {@link Job} instance to execute.
     */
    public void execute(@NonNull Job job) {
        eventBus.post(job);
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onEvent(@NonNull Job job) {
        JobResult jobResult = job.execute();
        eventBus.post(jobResult);
    }
}