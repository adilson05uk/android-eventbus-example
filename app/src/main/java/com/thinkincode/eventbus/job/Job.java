package com.thinkincode.eventbus.job;

import android.support.annotation.NonNull;

/**
 * Represents a job that is to be scheduled and executed in a background thread.
 *
 * @param <JR> the return type of the job.
 */
public interface Job<JR extends JobResult> {

    /**
     * Executes the job.
     *
     * @return the result of the job execution.
     */
    @NonNull
    JR execute();
}