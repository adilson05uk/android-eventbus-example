package com.thinkincode.eventbus.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.thinkincode.eventbus.MyApplication;
import com.thinkincode.eventbus.R;
import com.thinkincode.eventbus.job.a.JobA;
import com.thinkincode.eventbus.job.b.JobB;
import com.thinkincode.eventbus.job.JobExecutor;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private JobExecutor jobExecutor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        jobExecutor = MyApplication.getInstance().getJobExecutor();
    }

    @Override
    protected void onResume() {
        super.onResume();
        jobExecutor.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        jobExecutor.unregister(this);
    }

    @OnClick(R.id.buttonExecuteJobA)
    void executeJobA() {
        Toast.makeText(this, "Scheduling JobA for execution...", Toast.LENGTH_SHORT).show();
        jobExecutor.execute(new JobA());
    }

    @OnClick(R.id.buttonExecuteJobB)
    void executeJobB() {
        Toast.makeText(this, "Scheduling JobB for execution...", Toast.LENGTH_SHORT).show();
        jobExecutor.execute(new JobB());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onJobACompleted(JobA.Result result) {
        Toast.makeText(this, "Received JobA.Result: " + result.getValue(), Toast.LENGTH_SHORT).show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onJobBCompleted(JobB.Result result) {
        Toast.makeText(this, "Received JobB.Result: " + result.getValue(), Toast.LENGTH_SHORT).show();
    }
}